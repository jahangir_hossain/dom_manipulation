
function addItem(){
    var itemName = document.getElementById("itemName").value;

    console.log(itemName);

    var parent = document.createElement('div');
    parent.setAttribute('class','row justify-content-between');
    
    var childOne = document.createElement('div');
    childOne.setAttribute('class', 'col-4');
    childOne.innerHTML = itemName;

    var childTwo = document.createElement('div');
    childTwo.setAttribute('class', 'col-auto')

    var deleteBtn = document.createElement('button');
    deleteBtn.setAttribute('type', 'button');
    deleteBtn.textContent = 'Delete';
    deleteBtn.setAttribute('class', 'btn btn-sm btn-danger');
    deleteBtn.setAttribute('onclick', 'deleteItem(this)');

    childTwo.appendChild(deleteBtn);
    parent.appendChild(childOne);
    parent.appendChild(childTwo);

    var ulElement = document.getElementById('itemsList');
    console.log(ulElement);
    var liElement = document.createElement('li')
    liElement.setAttribute('class', 'list-group-item list-group-item-primary');
    liElement.appendChild(parent);

    ulElement.append(liElement)
    document.getElementById("itemName").value = '';
    document.getElementById("itemName").focus();

}

function deleteItem(e){
    console.log(e);
    e.parentNode.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode.parentNode);
    console.log(e);
    document.getElementById("itemName").focus();


}